<?php

use App\Models\Produk;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ArtikelController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/produk', [ProdukController::class, 'index']);
Route::get('/video', [VideoController::class, 'index']);
Route::get('/artikel', [ArtikelController::class, 'index']);
Route::get('/tentang-kami', [HomeController::class, 'tentangkami']);


Route::get('/login', [HomeController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [HomeController::class, 'authenticate']);
Route::get('/signup', [HomeController::class, 'register'])->middleware('guest');
Route::post('/signup', [HomeController::class, 'store']);
Route::post('/logout', [HomeController::class, 'logout']);

Route::get('/dashboard', [HomeController::class, 'dashboard'])->middleware('auth');
Route::get('/dashboard/coba', [HomeController::class, 'coba'])->middleware('auth');


//produk
Route::get('/dashboard/data-produk', [ProdukController::class, 'dataproduk'])->middleware('auth');
Route::get('/dashboard/tambah-data-produk', [ProdukController::class, 'tambahdataproduk'])->middleware('auth');
Route::post('/dashboard/tambah-data-produk', [ProdukController::class, 'tambahdata'])->middleware('auth');

// artikel
Route::get('/dashboard/data-artikel', [ArtikelController::class, 'dataartikel'])->middleware('auth');
Route::get('/dashboard/tambah-data-artikel', [ArtikelController::class, 'tambahdataartikel'])->middleware('auth');
Route::post('/dashboard/tambah-data-artikel', [ArtikelController::class, 'store'])->middleware('auth');
Route::patch('/dashboard/data-artikel/{id}', [ArtikelController::class, 'editdataartikel'])->middleware('auth');
Route::get('/dashboard/data-artikel/edit/{id}', [ArtikelController::class, 'edit'])->middleware('auth');
Route::delete('/dashboard/data-artikel/{artikel:id}', [ArtikelController::class, 'delete'])->name('hapusdata')->middleware('auth');
Route::get('/dashboard/tambah-data-artikel/createSlug', [ArtikelController::class, 'createSlug'])->middleware('auth');

//video
Route::get('/dashboard/data-video', [VideoController::class, 'datavideo'])->middleware('auth');
Route::get('/dashboard/tambah-data-video', [VideoController::class, 'tambahdatavideo'])->middleware('auth');
Route::head('/dashboard/tambah-data-video', [VideoController::class, 'store'])->middleware('auth');
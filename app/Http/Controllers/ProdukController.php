<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\User;
use App\Models\Artikel;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class ProdukController extends Controller
{
    public function index()
    {
        return view('produk', [
            'title' => 'All Product',
            'posts' => Produk::latest()->get()
        ]);
    }

    public function dataproduk()
    {
        return view('dashboard/produk/dataproduk', [
            'title' => 'Data Produk',
            'produk' => Produk::latest()->get()
        ]);
    }

    public function tambahdataproduk()
    {
        return view('dashboard.produk.tambah-data-produk', [
            'title' => 'Tambah Data Produk',
            'produk' => Produk::latest()->get(),
            'kategori' => Kategori::all()
        ]);
    }

    public function store(Request $request)
    {

        dd($request);
        // $validatedData =  $request->validate([
        //     'nama' => 'required|max:255',
        //     'slug' => 'required|unique:artikels',
        //     'kategori_id' => 'required',
        //     'deskripsi' => 'required',
        //     'foto' => 'image|file|max:10240',
        //     'berat' => 'required',
        //     'harga' => 'required',
        // ]);

        // $validatedData['user_id'] = auth()->user()->id;

        // if ($request->file('foto')) {
        //     $validatedData['foto'] = $request->file('foto')->store('foto-video');
        // }

        // Produk::create($validatedData);

        // return redirect('/dashboard/data-video')->with('success', 'Data berhasil ditambahkan!');
    }
}
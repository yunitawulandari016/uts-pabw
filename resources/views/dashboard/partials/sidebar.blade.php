<aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
  <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
    <i class="fe fe-x"><span class="sr-only"></span></i>
  </a>
  <nav class="vertnav navbar navbar-light">
    <!-- nav bar -->
    <div class="w-100 mb-4 d-flex">
      <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="/dashboard">
        <img src="{{ asset('dashboard-assets/assets/images/logo-pav.png') }}" width="40px" alt="">
      </a>
    </div>
    <ul class="navbar-nav flex-fill w-100 mb-2">
      <li class="nav-item dropdown">
        <a href="/dashboard" class="nav-link">
          <i class="fe fe-home fe-16"></i>
          <span class="ml-3 item-text">Dashboard</span><span class="sr-only">(current)</span>
        </a>
        </ul>
      </li>
    </ul>
    <p class="text-muted nav-heading mt-4 mb-1">
      <span>Components</span>
    </p>
    <ul class="navbar-nav flex-fill w-100 mb-2">
      <li class="nav-item dropdown">
        <a href="#ui-elements" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
          <i class="fe fe-box fe-16"></i>
          <span class="ml-3 item-text">Artikel</span>
        </a>
        <ul class="collapse list-unstyled pl-4 w-100" id="ui-elements">
          <li class="nav-item">
            <a class="nav-link pl-3" href="/dashboard/data-artikel"><span class="ml-1 item-text">Data Tabel Artikel</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link pl-3" href="/dashboard/tambah-data-artikel"><span class="ml-1 item-text">Tambah Data</span></a>
          </li>
        </ul>
      </li>
      <li class="nav-item dropdown">
        <a href="#forms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
          <i class="fe fe-credit-card fe-16"></i>
          <span class="ml-3 item-text">Video</span>
        </a>
        <ul class="collapse list-unstyled pl-4 w-100" id="forms">
          <li class="nav-item">
            <a class="nav-link pl-3" href="/dashboard/data-video"><span class="ml-1 item-text">Data Tabel Video</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link pl-3" href="/dashboard/tambah-data-video"><span class="ml-1 item-text">Tambah Data</span></a>
          </li>
        </ul>
      </li>
      <li class="nav-item dropdown">
        <a href="#tables" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
          <i class="fe fe-grid fe-16"></i>
          <span class="ml-3 item-text">Produk</span>
        </a>
        <ul class="collapse list-unstyled pl-4 w-100" id="tables">
          <li class="nav-item">
            <a class="nav-link pl-3" href="/dashboard/data-produk"><span class="ml-1 item-text">Data Tabel Produk</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link pl-3" href="/dashboard/tambah-data-produk"><span class="ml-1 item-text">Tambah Data</span></a>
          </li>
        </ul>
      </li>
    </ul>
    <div class="btn-box w-100 mt-4 mb-1">
      <a href="https://themeforest.net/item/tinydash-bootstrap-html-admin-dashboard-template/27511269" target="_blank" class="btn mb-2 btn-danger btn-lg btn-block">
        <span class="small">Log Out</span>
      </a>
    </div>
  </nav>
</aside>
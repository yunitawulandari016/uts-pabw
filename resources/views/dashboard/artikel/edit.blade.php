@extends('dashboard.layout.main')



@section('container')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2 class="page-title">Edit Data Artikel</h2>
            <p class="text-muted">Demo for form control styles, layout options, and custom components for
                creating a wide variety of forms.</p>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Form Artikel</strong>
                </div>
                <div class="card-body">
                    <form class="row" method="POST" action="/dashboard/data-artikel/{{ $id_artikel }}">
                        @csrf
                        @method('patch')
                        <input type="text" value="{{ $id_artikel }}" hidden>
                        <div class="col-md-12">
                            <div class="form-group mb-3">
                                <label for="simpleinput">Judul</label>
                                <input type="text" id="nama" name="nama" value="{{ $judul_artikel }}" class="form-control">
                                @error('nama')
                          <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                        @enderror
                        <div class="form-group mb-3 mt-3">
                            <label for="slug">Slug</label>
                            <input value="{{ $judul_artikel  }}" type="slug" id="slug" name="slug"
                                class="form-control">
                                @error('slug')
                      <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                    @enderror
                        </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="kategori_id">Kategori</label>
                                <select class="custom-select" id="kategori_id" name="kategori_id">
                                    <option >pilih</option>
                                    @foreach ($kategori as $kat)
                                        @if (intval($kategori_artikel) === $kat['id'])
                                            <option value="{{ $kat['id'] }}" selected>{{ $kat['nama'] }}</option>
                                        @else
                                            <option value="{{ $kat['id'] }}">{{ $kat['nama'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('kategori_id')
                          <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                        @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea id="deskripsi" class="form-control" name="deskripsi" rows="10" cols="50">{{  $deskripsi_artikel }}</textarea>
                                @error('deskripsi')
                          <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                        @enderror
                            </div>
                            <button class="btn btn-primary" type="submit">Edit Data</button>
                        </div> 
                    </form>
                </div> <!-- / .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> 
</div>

<script>
    cpnst nama = document.querySelector('#nama');
    cpnst slug = document.querySelector('#slug');

    nama.addEventListener('change', function(){
        fetch('/dashboard/tambah-data-artikel/createSlug?nama=' + nama.slug())
        .then((response) => response.json())
        .then((data) => slug.value = data.slug);
    });
</script>

@endsection